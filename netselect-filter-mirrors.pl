#!/usr/bin/env perl
use strict;
use warnings;
use File::Basename;

# Get parameters and fill in defaults.
my %params = (
	filename		=> $ARGV[0] || 'mirrors'
	,arch			=> $ARGV[1] || 'amd64'
	,protocol	=> $ARGV[2] || 'HTTP'
	,search		=> $ARGV[3] || 'Packages over'
	,ofilename	=> ''
);

sub help {
	my $name = basename($0);
	print "Usage: $name <arguments>\n"
	. "Where <arguments> can be:\n"
	. "\t'-h' || '--help'\tTo display this help.\n"
	. "OR\n\t<input file> <arch> <protocol> <search term>]\n"
	. "Where:\n"
	. "\t<input file> is the filename to parse as html."
	. " (Default='$params{filename}')\n"
	. "\t<arch> is the architecture of interest."
	. " (Default='$params{arch}')\n"
	. "\t<protocol> is the protocol used."
	. " (Default='$params{protocol}')\n"
	. "\t<search term> is the heading to look for."
	. " (Default='$params{search}')\n"
	. "\tYou can use '0' as placeholder to use the default for skipped parameters,\n"
	. "\teg.: '0 i386' to only change <arch> while using defaults for the rest.\n"
	;
	exit;
}

sub makeFilteredList(){
	# Compose output filename.
	$params{ofilename} = "$params{filename}.filtered-list.txt";

	open(my $ifh, '<:encoding(UTF-8)', $params{filename})
		or die "Could not open file: '$params{filename}' for reading !\n$!";

	open(my $ofh, '>:encoding(UTF-8)', $params{ofilename})
		or die "Could not open file: '$params{ofilename}' for writing !\n$!";

	$/="<br><br>";
	my $regExpHeading = "Includes architectures:";
	my $regExpHeadingFull
			= $regExpHeading
			. ".+$params{arch}.+"
			;
	my $lineRegExp
			= "$params{search} $params{protocol}:"
			. ".*<a rel=\"nofollow\" href=\"("
			. "$params{protocol}://.*?"
			. ")\">"
			;
	while(<$ifh>){
		next if $_ !~ /Site:/;
		if(
			(
				/$regExpHeadingFull/i
				||
				$_ !~ /$regExpHeading/
			)
			&&
			m@$lineRegExp@i
		){
			say $ofh "$1";
		}
	}
	close $ofh;
	close $ifh;
}

# Display help when...
&help() if ($#ARGV + 1 == 0 || $#ARGV + 1 > 4);
&help() if ($ARGV[0] eq '-h' || $ARGV[0] eq '--help');
# Ok to parse
&makeFilteredList()
__END__
